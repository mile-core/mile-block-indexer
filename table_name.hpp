constexpr const char *table_name_blocks = "blocks";
constexpr const char *table_name_wallets = "wallets";
constexpr const char *table_name_transactions = "transactions";
constexpr const char *table_name_blockchain_state = "blockchain_state";
constexpr const char *table_name_nodes_state = "nodes_state";
constexpr const char *table_name_transaction_stream = "transaction_stream";