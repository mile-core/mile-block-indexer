cmake_minimum_required(VERSION 3.9)

project("mile-block-indexer" CXX)

set(CMAKE_CXX_STANDARD 17)

find_package(Boost REQUIRED COMPONENTS unit_test_framework regex program_options system)

find_package(Threads)

include_directories (
        ${Boost_INCLUDE_DIRS}
)


set(yaml_path thirdparty/yaml-cpp)
find_package(${YAML_INCLUDE_DIR} REQUIRED PATHS ${yaml_path} )
include_directories(SYSTEM thirdparty/yaml-cpp/include)

find_program(CCACHE_FOUND ccache)
if(CCACHE_FOUND)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_COMPILE ccache)
    set_property(GLOBAL PROPERTY RULE_LAUNCH_LINK ccache)
endif(CCACHE_FOUND)

find_package(OpenSSL)
add_subdirectory(thirdparty)

include_directories(
        thirdparty/librethinkdbxx/build/include/
        thirdparty/json/include/
        thirdparty/mile-csa-jsonrpc-client/include
        ${OPENSSL_INCLUDE_DIR}
)

link_directories(${PROJECT_SOURCE_DIR}/thirdparty/librethinkdbxx/build)

set(HEADER
        utility.hpp
        wrapper.hpp
        block_processing.hpp
)

set(SOURCE

        block-indexer.cpp
        block_processing.cpp
        utility.cpp
)

add_executable(${PROJECT_NAME} ${HEADER}  ${SOURCE} )

target_link_libraries(
        ${PROJECT_NAME}
        ${CMAKE_THREAD_LIBS_INIT}
        ${Boost_LIBRARIES}
        ${OPENSSL_SSL_LIBRARY}
        ${OPENSSL_CRYPTO_LIBRARY}
        yaml-cpp
        librethinkdb++.a
        milecsa
        milecsa_jsonrpc
        dl
)

