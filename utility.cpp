#include "utility.hpp"
#include "crypto_types.h"

auto to_base58(std::string key_hex) ->std::string {
    std::string error;
    PublicKey key;
    key.SetHexString(key_hex,error);
    return key.ToBase58CheckString();

}

/*
void to_json(nlohmann::json &block_history_state, const pcsa_blockchain_state &state) {
    block_history_state["block_count"] = state.block_count;
    block_history_state["last_block_digest"] = state.last_block_digest;
    block_history_state["last_block_merkle_root"] = state.last_block_merkle_root;
    block_history_state["transaction_count"] = state.transaction_count;
    block_history_state["node_count"] = state.node_count;
    block_history_state["local_node"] = state.local_node;
    block_history_state["non_empty_wallet_count"] = state.non_empty_wallet_count;
    block_history_state["voting_transaction_count"] = state.voting_transaction_count;
    block_history_state["pending_transaction_count"] = state.pending_transaction_count;
    block_history_state["blockchain_state"] = state.blockchain_state;

    if(state.synchronization_state != nullptr){
        block_history_state["synchronization_state"] = nlohmann::json::parse(std::string(state.synchronization_state));
    }

    block_history_state["consensus_round"] = state.consensus_round;

    if(state.synchronization_state != nullptr) {
        block_history_state["voting_nodes"] = nlohmann::json::parse(std::string(state.voting_nodes));
    }
}
 */

bool in(const std::string &element, std::unordered_set<std::string> &container) {
    return container.find(element) != container.end();
}

auto to_json(const std::string &raw_block) -> nlohmann::json {
    auto block =  nlohmann::json::parse(raw_block);

    /*
    if(block["signature"].is_array()) {

        for (auto &i:block.at("signature")) {
            i["producerPublicKey"] = to_base58(i["producerPublicKey"].get<std::string>());
            i["producerSignature"] = to_base58(i["producerSignature"].get<std::string>());

        }
    }*/

    block["transaction-count"] = std::stoull(block["transaction-count"].get<std::string>());
    ///block["timestamp"] = std::stoull(block["timestamp"].get<std::string>());
    block["version"] = std::stoull(block["version"].get<std::string>());
/*
    if(block.at("transactions").is_array()) {
        for (auto &i:block.at("transactions")) {
            if (!i["destinationWalletPublicKey"].is_null()) {
                i["destinationWalletPublicKey"] = to_base58(i["destinationWalletPublicKey"].get<std::string>());
            }


            if (!i["sourceWalletPublicKey"].is_null()) {
                i["sourceWalletPublicKey"] = to_base58(i["sourceWalletPublicKey"].get<std::string>());
            }

            if (!i["assetCode"].is_null()) {
                i["assetCode"] = std::stoull(i["assetCode"].get<std::string>());
            }

            if (!i["code"].is_null()) {
                i["code"] = std::stoull(i["code"].get<std::string>());
            }

            if (!i["id"].is_null()) {
                i["id"] = std::stoull(i["id"].get<std::string>());
            }


            if (!i["signed"].is_null()) {
                auto signed_ = i["signed"];
                if ("true" == signed_) {
                    i["signed"] = true;
                } else {
                    i["signed"] = false;
                }

            }


        }

    }

*/

    return block;
}
