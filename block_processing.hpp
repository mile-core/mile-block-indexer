#pragma once

#include <rethinkdb.h>

#include <json.hpp>

namespace R = RethinkDB;

void process_block(R::Connection&conn, std::string  block_data, const std::string& db_name,nlohmann::json&json_state);

void process_trx(R::Connection &conn,const std::string& db_name,nlohmann::json&);

void update_nodes_state(R::Connection &conn,const std::string&, nlohmann::json & , nlohmann::json &);