#include <iostream>

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio.hpp>
#include <boost/program_options.hpp>

#include <rethinkdb.h>

#include <yaml-cpp/yaml.h>
#include <json.hpp>

#include "crypto_types.h"

#include "wrapper.hpp"
#include "utility.hpp"
#include "block_processing.hpp"
#include "table_name.hpp"

#include "milecsa_jsonrpc.hpp"

namespace R = RethinkDB;

namespace po = boost::program_options;

using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
namespace http = boost::beast::http;    // from <boost/beast/http.hpp>



#ifdef __APPLE__

#else
#include <boost/stacktrace.hpp>

void terminate_handler() {
    std::cerr << "terminate called:"
              << std::endl
              << boost::stacktrace::stacktrace()
              << std::endl;
}

void signal_sigsegv(int signum){
    boost::stacktrace::stacktrace bt ;
    if(bt){
        std::cerr << "SIgnal"
                  << signum
                  << " , backtrace:"
                  << std::endl
                  << boost::stacktrace::stacktrace()
                  << std::endl;
    }
    std::abort();
}

#endif





int main(int argc, char **argv) {

#ifdef __APPLE__

#else
    ::signal(SIGSEGV,&signal_sigsegv);

    std::set_terminate(terminate_handler);
#endif

    boost::program_options::variables_map args_;
    boost::program_options::options_description app_options_;

    app_options_.add_options()
            ("help", "")
            ("config", po::value<std::string>(), "config");
    boost::program_options::store(boost::program_options::parse_command_line(argc, argv, app_options_), args_);

    po::notify(args_);

    if (args_.count("help")) {
        std::cout << app_options_ << std::endl;
        return 0;
    }



    std::string node_ip;///: "165.227.135.104"
    int node_port;///: 4010
    std::string user_name;///: "testnet"
    std::string client;///: "web"
    std::string rethink_db_ip;//: "172.17.0.2"
    int rethink_db_port;///: 28015
    std::string db_name;///: "blockchain"
    int read_timeout;///: 60

    std::string node_address;
    auto const  port = "80";
    auto const  target = "";

    boost::asio::io_context ioc;

    tcp::resolver resolver{ioc};

    if (args_.count("config")) {

        auto config_path = args_["config"].as<std::string>();

        YAML::Node yaml_config = YAML::LoadFile(config_path);

        node_address = yaml_config["node_address"].as<std::string>();
        node_port = yaml_config["node_port"].as<int>();
        user_name = yaml_config["user_name"].as<std::string>();
        client = yaml_config["client"].as<std::string>();
        rethink_db_ip = yaml_config["rethink_db_host"].as<std::string>();
        rethink_db_port = yaml_config["rethink_db_port"].as<int>();
        db_name = yaml_config["db_name"].as<std::string>();
        read_timeout = yaml_config["read_timeout"].as<int>();

    } else {
        std::cerr << "Not find config.yaml\n";
        return 1;
    }

    std::cerr << "Start Init Connected DB" << std::endl;

    auto conn = R::connect(rethink_db_ip, rethink_db_port);
    if (!conn) {
        std::cerr << "Could not connect to server\n";
        return 1;
    }

    std::cerr << "Finish Init Connected DB" << std::endl;


    std::cerr << "Start Table Init in  DB" << std::endl;

    bool init_db = true;
    R::Cursor databases = R::db_list().run(*conn);
    for (R::Datum const &db : databases) {
        if (*db.get_string() == db_name) {
            init_db = false;
            break;
        }
    }


    if (init_db) {
        /// create table
        R::db_create(db_name).run(*conn);
        R::db(db_name).table_create(table_name_wallets).run(*conn);
        R::db(db_name).table_create(table_name_blocks).run(*conn);
        R::db(db_name).table_create(table_name_transactions).run(*conn);
        R::db(db_name).table_create(table_name_blockchain_state).run(*conn);
        R::db(db_name).table_create(table_name_nodes_state).run(*conn);
        R::db(db_name).table_create(table_name_transaction_stream).run(*conn);

        /// create index
        R::db(db_name).table(table_name_blocks).index_create("block-id").run(*conn);
        R::db(db_name).table(table_name_blockchain_state).index_create("block-id").run(*conn);
        R::db(db_name).table(table_name_nodes_state).index_create("block-id").run(*conn);
    }


    milecsa::ErrorHandler error_handler = [](milecsa::result code, std::string error){
       std::cerr <<"Request Error: " << error<<std::endl;

    };

    milecsa::http::ResponseHandler response_handler = [&](const milecsa::http::status code, const std::string &method, const milecsa::http::response &http){
        std::cerr << "Response["<<code<<"] "<<method<<" error: " << http.result() << std::endl << http << std::endl;
    };

    auto client_t = milecsa::rpc::Client::Connect(node_address,false,response_handler,error_handler);

    std::cerr << "Finish Table Init in  DB" << std::endl;


    std::cerr << "Start  mile connect" << std::endl;

    std::cerr << "Fnish mile connect" << std::endl;

    std::atomic_bool enabled(true);

    uint256_t start_position(0);

    if(!init_db) {
        try {
            auto cursore = R::db(db_name).table(table_name_blocks).max(R::optargs("index", "block-id"))["block-id"].run(*conn);

            start_position = static_cast<uint64_t>((*cursore.to_datum().get_number()));
        } catch (...) {
            start_position = 0;
        }

    }

    std::cerr<<" start_position = " << UInt256ToDecString(start_position)<<std::endl;

    while (enabled) {
        std::this_thread::sleep_for(std::chrono::seconds(read_timeout));
        {
            std::cerr << "Start Mile Read state " << std::endl;

            milecsa::rpc::response response_json;

            for(;;){
                response_json = client_t->get_blockchain_state();
                std::cerr << "re-try request" <<std::endl;
                if(response_json){
                    break;
                }
            }


            std::cerr << response_json.value().dump() <<std::endl;

            auto blockchain_state = response_json.value()["result"];

            uint256_t id_block=0;
            StringToUInt256(blockchain_state["block-count"].get<std::string>(),id_block,false);
            std::cerr << "Finish Mile Read state " << std::endl;
                std::cerr<<" start position id = "<<UInt256ToDecString(start_position)<<std::endl;
                std::cerr<<" finish id = "<<UInt256ToDecString(id_block)<<std::endl;
                std::cerr << "Start Mile Read Block " << std::endl;
                if(start_position!=id_block) {
                    {
                        for (; start_position < id_block; ++start_position) {
                            std::cerr << "start get block in network" << std::endl;
                            milecsa::rpc::response r = client_t->get_block(start_position);
                            std::cerr << "finish get block in network" << std::endl << std::endl;
                            std::cerr <<  r.value().dump() <<std::endl;
                            std::cerr << std::endl << std::endl;
                            auto block_json =  r.value()["result"]["block-data"];

                            std::cerr << block_json.dump() <<std::endl;

                            std::cerr << "Start Mile Read Block: process_block " << std::endl;
                            process_block(*conn.get(), block_json.dump(), db_name, blockchain_state);
                            process_trx(*conn.get(),db_name,block_json);

                        }

                        milecsa::rpc::response r = client_t->get_nodes();
                        update_nodes_state(*conn.get(),db_name,r.value(),response_json.value());

                    }
                }
                std::cerr << "Finish Mile Read Block " << std::endl;
                continue;

        }
    }

    return 0;
}
