#include <iostream>
#include "block_processing.hpp"
#include "utility.hpp"
#include "table_name.hpp"

void process_transactions_all(R::Connection&conn,const std::string& db_name,nlohmann::json& trx_){
    std::cerr << trx_.dump() <<std::endl;
    std::string public_key;

    if(!trx_["public-key"].is_null()){
        public_key = trx_["public-key"].get<std::string>();
    }

    if(!trx_["from"].is_null()){
        public_key = trx_["from"].get<std::string>();
    }

    nlohmann::json transaction;
    transaction.merge_patch(trx_);

    std::string id(public_key);
    id.append(":");
    id.append(trx_["transaction-id"].get<std::string>());

    transaction["id"] = id;
    transaction["transaction-id"] = std::stoull(trx_.at("transaction-id").get<std::string>());

    R::db(db_name).table(table_name_transactions).insert(R::json(transaction.dump())).run(conn);
}

void process_transactions_transfer_assets_transaction(R::Connection&conn,const std::string& db_name,const std::string& block_id,nlohmann::json& trx_){

    auto to = trx_["to"].get<std::string>();
    auto from = trx_["from"].get<std::string>();

    nlohmann::json transaction;
    transaction.merge_patch(trx_);

    std::string id(from);
    id.append(":");
    id.append(trx_["transaction-id"].get<std::string>());

    if ( from == to ) {
        /// query
        std::cerr << "Start Mile Read Block: process_block: process transactions: check status " << std::endl;
        auto result = R::db(db_name).table(table_name_wallets).get_all(from, R"({"index":"id"})").is_empty().run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: check status " << std::endl;
        auto status = *(result.to_datum().get_boolean());
        if (status) {
            nlohmann::json query;
            query["id"] = from;
            nlohmann::json blocks;
            blocks.push_back(block_id);
            query["blocks"] = blocks;
            nlohmann::json trx;
            trx.push_back(id);
            query["transactions"] = trx;
            /// query
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
            R::db(db_name).table(table_name_wallets).insert(R::json(query.dump())).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

        } else {
            nlohmann::json blocks;
            blocks.push_back(block_id);
            nlohmann::json trx;
            trx.push_back(id);
            /// query
            auto d = [&](R::Var ff) {
                return R::object(
                        "blocks",
                        (*ff)["blocks"].coerce_to("array").set_union(R::json(blocks.dump())),
                        "transactions",
                        (*ff)["transactions"].coerce_to("array").set_union(R::json(trx.dump()))
                );
            };
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "
                      << std::endl;
            R::db(db_name).table(table_name_wallets).get(from).update(d, R::optargs("non_atomic",true)).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

        }

    } else {
        /// query
        std::cerr << "Start Mile Read Block: process_block: process transactions: check status " << std::endl;
        auto result = R::db(db_name).table(table_name_wallets).get_all(from, R"({"index":"id"})").is_empty().run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: check status " << std::endl;
        auto status = *(result.to_datum().get_boolean());

        if (status) {
            nlohmann::json query;
            query["id"] = from;
            nlohmann::json blocks;
            blocks.push_back(block_id);
            query["blocks"] = blocks;
            nlohmann::json trx;
            trx.push_back(id);
            query["transactions"] = trx;
            /// query
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
            R::db(db_name).table(table_name_wallets).insert(R::json(query.dump())).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

        } else {
            nlohmann::json blocks;
            blocks.push_back(block_id);
            nlohmann::json trx;
            trx.push_back(id);
            /// query
            auto d = [&](R::Var ff) {
                return R::object(
                        "blocks",
                        (*ff)["blocks"].coerce_to("array").set_union(R::json(blocks.dump())),
                        "transactions",
                        (*ff)["transactions"].coerce_to("array").set_union(R::json(trx.dump()))
                );
            };
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
            R::db(db_name).table(table_name_wallets).get(from).update(d, R::optargs("non_atomic",true)).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
        }

        std::cerr << "Start Mile Read Block: process_block: process transactions: check status " << std::endl;
        result = R::db(db_name).table(table_name_wallets).get_all(to, R"({"index":"id"})").is_empty().run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: check status " << std::endl;
        status = *(result.to_datum().get_boolean());
        if (status) {
            nlohmann::json query;
            query["id"] = to;
            nlohmann::json blocks;
            blocks.push_back(block_id);
            query["blocks"] = blocks;
            nlohmann::json trx;
            trx.push_back(id);
            query["transactions"] = trx;
            /// query
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
            R::db(db_name).table(table_name_wallets).insert(R::json(query.dump())).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
        } else {
            nlohmann::json blocks;
            blocks.push_back(block_id);
            nlohmann::json trx;
            trx.push_back(id);
            /// query
            auto d = [&](R::Var ff) {
                return R::object(
                        "blocks",
                        (*ff)["blocks"].coerce_to("array").set_union(R::json(blocks.dump())),
                        "transactions",
                        (*ff)["transactions"].coerce_to("array").set_union(R::json(trx.dump()))
                );
            };
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
            R::db(db_name).table(table_name_wallets).get(to).update(d, R::optargs("non_atomic",true)).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

        }


    }
}

void process_transactions_emission_transaction(R::Connection&conn,const std::string& db_name,const std::string& block_id,nlohmann::json& trx_) {
    auto to = trx_["to"].get<std::string>();
    auto from = trx_["from"].get<std::string>();

    nlohmann::json transaction;
    transaction.merge_patch(trx_);

    std::string id(from);
    id.append(":");
    id.append(trx_["transaction-id"].get<std::string>());

    if (from == to) {
        /// query
        std::cerr << "Start Mile Read Block: process_block: process transactions: check status " << std::endl;
        auto result = R::db(db_name).table(table_name_wallets).get_all(from, R"({"index":"id"})").is_empty().run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: check status " << std::endl;
        auto status = *(result.to_datum().get_boolean());
        if (status) {
            nlohmann::json query;
            query["id"] = from;
            nlohmann::json blocks;
            blocks.push_back(block_id);
            query["blocks"] = blocks;
            nlohmann::json trx;
            trx.push_back(id);
            query["transactions"] = trx;
            /// query
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
            R::db(db_name).table(table_name_wallets).insert(R::json(query.dump())).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

        } else {
            nlohmann::json blocks;
            blocks.push_back(block_id);
            nlohmann::json trx;
            trx.push_back(id);
            /// query
            auto d = [&](R::Var ff) {
                return R::object(
                        "blocks",
                        (*ff)["blocks"].coerce_to("array").set_union(R::json(blocks.dump())),
                        "transactions",
                        (*ff)["transactions"].coerce_to("array").set_union(R::json(trx.dump()))
                );
            };
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "
                      << std::endl;
            R::db(db_name).table(table_name_wallets).get(from).update(d, R::optargs("non_atomic",true)).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

        }

    } else {
        /// query
        std::cerr << "Start Mile Read Block: process_block: process transactions: check status " << std::endl;
        auto result = R::db(db_name).table(table_name_wallets).get_all(from, R"({"index":"id"})").is_empty().run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: check status " << std::endl;
        auto status = *(result.to_datum().get_boolean());

        if (status) {
            nlohmann::json query;
            query["id"] = from;
            nlohmann::json blocks;
            blocks.push_back(block_id);
            query["blocks"] = blocks;
            nlohmann::json trx;
            trx.push_back(id);
            query["transactions"] = trx;
            /// query
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
            R::db(db_name).table(table_name_wallets).insert(R::json(query.dump())).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

        } else {
            nlohmann::json blocks;
            blocks.push_back(block_id);
            nlohmann::json trx;
            trx.push_back(id);
            /// query
            auto d = [&](R::Var ff) {
                return R::object(
                        "blocks",
                        (*ff)["blocks"].coerce_to("array").set_union(R::json(blocks.dump())),
                        "transactions",
                        (*ff)["transactions"].coerce_to("array").set_union(R::json(trx.dump()))
                );
            };
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
            R::db(db_name).table(table_name_wallets).get(from).update(d, R::optargs("non_atomic",true)).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
        }

        std::cerr << "Start Mile Read Block: process_block: process transactions: check status " << std::endl;
        result = R::db(db_name).table(table_name_wallets).get_all(to, R"({"index":"id"})").is_empty().run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: check status " << std::endl;
        status = *(result.to_datum().get_boolean());
        if (status) {
            nlohmann::json query;
            query["id"] = to;
            nlohmann::json blocks;
            blocks.push_back(block_id);
            query["blocks"] = blocks;
            nlohmann::json trx;
            trx.push_back(id);
            query["transactions"] = trx;
            /// query
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
            R::db(db_name).table(table_name_wallets).insert(R::json(query.dump())).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
        } else {
            nlohmann::json blocks;
            blocks.push_back(block_id);
            nlohmann::json trx;
            trx.push_back(id);
            /// query
            auto d = [&](R::Var ff) {
                return R::object(
                        "blocks",
                        (*ff)["blocks"].coerce_to("array").set_union(R::json(blocks.dump())),
                        "transactions",
                        (*ff)["transactions"].coerce_to("array").set_union(R::json(trx.dump()))
                );
            };
            std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
            R::db(db_name).table(table_name_wallets).get(to).update(d, R::optargs("non_atomic",true)).run(conn);
            std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

        }


    }
}


void process_transactions_create_poll_set_token_course(R::Connection&conn,const std::string& db_name,const std::string& block_id,nlohmann::json& trx_) {

    auto public_key = trx_["public-key"].get<std::string>();

    nlohmann::json transaction;
    transaction.merge_patch(trx_);

    std::string id(public_key);
    id.append(":");
    id.append(trx_["transaction-id"].get<std::string>());

    /// query
    std::cerr << "Start Mile Read Block: process_block: process transactions: check status " << std::endl;
    auto result = R::db(db_name).table(table_name_wallets).get_all(public_key, R"({"index":"id"})").is_empty().run(conn);
    std::cerr << "Finish Mile Read Block: process_block: process transactions: check status " << std::endl;
    auto status = *(result.to_datum().get_boolean());
    if (status) {
        nlohmann::json query;
        query["id"] = public_key;
        nlohmann::json blocks;
        blocks.push_back(block_id);
        query["blocks"] = blocks;
        nlohmann::json trx;
        trx.push_back(id);
        query["transactions"] = trx;
        /// query
        std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
        R::db(db_name).table(table_name_wallets).insert(R::json(query.dump())).run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

    } else {
        nlohmann::json blocks;
        blocks.push_back(block_id);
        nlohmann::json trx;
        trx.push_back(id);
        /// query
        auto d = [&](R::Var ff) {
            return R::object(
                    "blocks",
                    (*ff)["blocks"].coerce_to("array").set_union(R::json(blocks.dump())),
                    "transactions",
                    (*ff)["transactions"].coerce_to("array").set_union(R::json(trx.dump()))
            );
        };
        std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "
                  << std::endl;
        R::db(db_name).table(table_name_wallets).get(public_key).update(d, R::optargs("non_atomic",true)).run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

    }

}

void process_transactions_voting_course_count(R::Connection&conn,const std::string& db_name,const std::string& block_id,nlohmann::json& trx_) {
    auto public_key = trx_["public-key"].get<std::string>();

    nlohmann::json transaction;
    transaction.merge_patch(trx_);

    std::string id(public_key);
    id.append(":");
    id.append(trx_["transaction-id"].get<std::string>());


    /// query
    std::cerr << "Start Mile Read Block: process_block: process transactions: check status " << std::endl;
    auto result = R::db(db_name).table(table_name_wallets).get_all(public_key, R"({"index":"id"})").is_empty().run(conn);
    std::cerr << "Finish Mile Read Block: process_block: process transactions: check status " << std::endl;
    auto status = *(result.to_datum().get_boolean());
    if (status) {
        nlohmann::json query;
        query["id"] = public_key;
        nlohmann::json blocks;
        blocks.push_back(block_id);
        query["blocks"] = blocks;
        nlohmann::json trx;
        trx.push_back(id);
        query["transactions"] = trx;
        /// query
        std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
        R::db(db_name).table(table_name_wallets).insert(R::json(query.dump())).run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

    } else {
        nlohmann::json blocks;
        blocks.push_back(block_id);
        nlohmann::json trx;
        trx.push_back(id);
        /// query
        auto d = [&](R::Var ff) {
            return R::object(
                    "blocks",
                    (*ff)["blocks"].coerce_to("array").set_union(R::json(blocks.dump())),
                    "transactions",
                    (*ff)["transactions"].coerce_to("array").set_union(R::json(trx.dump()))
            );
        };
        std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "
                  << std::endl;
        R::db(db_name).table(table_name_wallets).get(public_key).update(d, R::optargs("non_atomic",true)).run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

    }
}

void process_transactions_voting_course_poll(R::Connection&conn,const std::string& db_name,const std::string& block_id,nlohmann::json& trx_) {
    auto public_key = trx_["public-key"].get<std::string>();

    nlohmann::json transaction;
    transaction.merge_patch(trx_);

    std::string id(public_key);
    id.append(":");
    id.append(trx_["transaction-id"].get<std::string>());

    /// query
    std::cerr << "Start Mile Read Block: process_block: process transactions: check status " << std::endl;
    auto result = R::db(db_name).table(table_name_wallets).get_all(public_key, R"({"index":"id"})").is_empty().run(conn);
    std::cerr << "Finish Mile Read Block: process_block: process transactions: check status " << std::endl;
    auto status = *(result.to_datum().get_boolean());
    if (status) {
        nlohmann::json query;
        query["id"] = public_key;
        nlohmann::json blocks;
        blocks.push_back(block_id);
        query["blocks"] = blocks;
        nlohmann::json trx;
        trx.push_back(id);
        query["transactions"] = trx;
        /// query
        std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
        R::db(db_name).table(table_name_wallets).insert(R::json(query.dump())).run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

    } else {
        nlohmann::json blocks;
        blocks.push_back(block_id);
        nlohmann::json trx;
        trx.push_back(id);
        /// query
        auto d = [&](R::Var ff) {
            return R::object(
                    "blocks",
                    (*ff)["blocks"].coerce_to("array").set_union(R::json(blocks.dump())),
                    "transactions",
                    (*ff)["transactions"].coerce_to("array").set_union(R::json(trx.dump()))
            );
        };
        std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "
                  << std::endl;
        R::db(db_name).table(table_name_wallets).get(public_key).update(d, R::optargs("non_atomic",true)).run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

    }
}


void process_transactions_unregister_node_transaction(R::Connection&conn,const std::string& db_name,const std::string& block_id,nlohmann::json& trx_) {
    auto public_key = trx_["public-key"].get<std::string>();

    nlohmann::json transaction;
    transaction.merge_patch(trx_);

    std::string id(public_key);
    id.append(":");
    id.append(trx_["transaction-id"].get<std::string>());

    /// query
    std::cerr << "Start Mile Read Block: process_block: process transactions: check status " << std::endl;
    auto result = R::db(db_name).table(table_name_wallets).get_all(public_key, R"({"index":"id"})").is_empty().run(conn);
    std::cerr << "Finish Mile Read Block: process_block: process transactions: check status " << std::endl;
    auto status = *(result.to_datum().get_boolean());
    if (status) {
        nlohmann::json query;
        query["id"] = public_key;
        nlohmann::json blocks;
        blocks.push_back(block_id);
        query["blocks"] = blocks;
        nlohmann::json trx;
        trx.push_back(id);
        query["transactions"] = trx;
        /// query
        std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
        R::db(db_name).table(table_name_wallets).insert(R::json(query.dump())).run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

    } else {
        nlohmann::json blocks;
        blocks.push_back(block_id);
        nlohmann::json trx;
        trx.push_back(id);
        /// query
        auto d = [&](R::Var ff) {
            return R::object(
                    "blocks",
                    (*ff)["blocks"].coerce_to("array").set_union(R::json(blocks.dump())),
                    "transactions",
                    (*ff)["transactions"].coerce_to("array").set_union(R::json(trx.dump()))
            );
        };
        std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "
                  << std::endl;
        R::db(db_name).table(table_name_wallets).get(public_key).update(d, R::optargs("non_atomic",true)).run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

    }
}

void process_transactions_register_node_transaction_with_amount(R::Connection&conn,const std::string& db_name,const std::string& block_id,nlohmann::json& trx_) {
    auto public_key = trx_["public-key"].get<std::string>();

    nlohmann::json transaction;
    transaction.merge_patch(trx_);

    std::string id(public_key);
    id.append(":");
    id.append(trx_["transaction-id"].get<std::string>());

    /// query
    std::cerr << "Start Mile Read Block: process_block: process transactions: check status " << std::endl;
    auto result = R::db(db_name).table(table_name_wallets).get_all(public_key, R"({"index":"id"})").is_empty().run(conn);
    std::cerr << "Finish Mile Read Block: process_block: process transactions: check status " << std::endl;
    auto status = *(result.to_datum().get_boolean());
    if (status) {
        nlohmann::json query;
        query["id"] = public_key;
        nlohmann::json blocks;
        blocks.push_back(block_id);
        query["blocks"] = blocks;
        nlohmann::json trx;
        trx.push_back(id);
        query["transactions"] = trx;
        /// query
        std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;
        R::db(db_name).table(table_name_wallets).insert(R::json(query.dump())).run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

    } else {
        nlohmann::json blocks;
        blocks.push_back(block_id);
        nlohmann::json trx;
        trx.push_back(id);
        /// query
        auto d = [&](R::Var ff) {
            return R::object(
                    "blocks",
                    (*ff)["blocks"].coerce_to("array").set_union(R::json(blocks.dump())),
                    "transactions",
                    (*ff)["transactions"].coerce_to("array").set_union(R::json(trx.dump()))
            );
        };
        std::cerr << "Start Mile Read Block: process_block: process transactions: insert wallet "
                  << std::endl;
        R::db(db_name).table(table_name_wallets).get(public_key).update(d, R::optargs("non_atomic",true)).run(conn);
        std::cerr << "Finish Mile Read Block: process_block: process transactions: insert wallet "<< std::endl;

    }
}

void process_block(R::Connection&conn, std::string  block_data, const std::string& db_name,nlohmann::json&json_state) {
    std::cerr << "Start Mile Read Block:  process_block" << std::endl;

    auto block = to_json(block_data);

    block["block-id"] = std::stoull(block.at("id").get<std::string>());

    std::cerr << "Start Mile Read Block: process_block: process transactions" << std::endl;

    if( block.at("transactions").is_array() ) {

        for ( auto &i:block.at("transactions") ) {

            process_transactions_all(conn,db_name,i);

            if ( "TransferAssetsTransaction" == i["transaction-name"] ) {

                process_transactions_transfer_assets_transaction(conn,db_name,block.at("id").get<std::string>(),i);

            }

            if ( "EmissionTransaction" == i["transaction-name"] ) {

                process_transactions_emission_transaction(conn,db_name,block.at("id").get<std::string>(),i);

            }

            if ( "CreatePollSetTokenCourse" == i["transaction-name"] ) {

                process_transactions_create_poll_set_token_course(conn,db_name,block.at("id").get<std::string>(),i);

            }

            if ( "VotingCourseCount" == i["transaction-name"] ) {

                process_transactions_voting_course_count(conn,db_name,block.at("id").get<std::string>(),i);

            }


            if ( "VotingCoursePoll" == i["transaction-name"] ) {

                process_transactions_voting_course_poll(conn,db_name,block.at("id").get<std::string>(),i);

            }

            if ( "UnregisterNodeTransaction" == i["transaction-name"] ) {

                process_transactions_unregister_node_transaction(conn,db_name,block.at("id").get<std::string>(),i);

            }


            if ( "RegisterNodeTransactionWithAmount" == i["transaction-name"] ) {

                process_transactions_register_node_transaction_with_amount(conn,db_name,block.at("id").get<std::string>(),i);

            }

        }
    }

    std::cerr << "Finish Mile Read Block: process_block: process transactions" << std::endl;

    std::cerr << "Start Mile Read Block: process_block: process insert block" << std::endl;
    R::db(db_name).table(table_name_blocks).insert(R::json(block.dump())).run(conn);
    std::cerr << "Finish Mile Read Block: process_block: process insert block" << std::endl;

    std::cerr << "Start Mile Read Block: process_block: process insert state" << std::endl;

    json_state["id"] = block.at("id");

    json_state["block-id"] = std::stoull(block.at("id").get<std::string>());
    R::db(db_name).table(table_name_blockchain_state).insert(R::json(json_state.dump())).run(conn);
    std::cerr << "Finish Mile Read Block: process_block: process insert state" << std::endl;

    std::cerr << "Finish Mile Read Block:  process_block " << std::endl;

}

void update_nodes_state(R::Connection &conn,const std::string& db_name, nlohmann::json &nodes_state , nlohmann::json &state) {
    nlohmann::json query;
    nlohmann::json nodes;
    for(auto&i:nodes_state.at("result")){
        nodes.emplace_back(i);
    }
    query["nodes"] = nodes;

    auto result = state["result"];

    auto block_id = std::stoull(result["block-count"].get<std::string>());

    query.emplace("nodes",nodes);
    query.emplace("id",block_id);

    R::db(db_name).table(table_name_nodes_state).insert(R::json(query.dump())).run(conn);

}

void process_trx(R::Connection &conn, const std::string &db_name, nlohmann::json &block) {
    nlohmann::json query;
    nlohmann::json trx;

    if(block.at("transactions").is_array()){
        trx = block.at("transactions");
    } else {
        trx = nlohmann::json::array();
    }

    query["transactions"] = trx;
    query["id"]= std::stoull(block.at("id").get<std::string>());

    R::db(db_name).table(table_name_transaction_stream).insert(R::json(query.dump())).run(conn);
}