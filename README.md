# MILE Blockchain Indexer 

## Requirements
1. c++17
1. cmake >= 3.9
1. boost >= 1.66.0 
1. openssl == 1.0.2p
1. docker
1. [RethinkDB](https://rethinkdb.com)
1. [Mile CSA API](https://github.com/mile-core/mile-csa-api)
1. [Mile CSA JSON-RPC API](https://github.com/mile-core/mile-csa-jsonrpc-client)

## Build on Ubuntu 18.04 (boost version must be updated to 1.66.0-1.67.0)

    $ git clone https://bitbucket.org/mile-core/mile-block-indexer  
    $ cd mile-block-indexer
    $ git submodule update --init --recursive --remote
    $ cd thirdparty/librethinkdbxx && make; cd ../..;
    $ mkdir build; cd build; cmake ..; make -j4
    
## Build with user defined boost and open ssl paths
    $ cmake CMakeLists.txt  -DBoost_NO_BOOST_CMAKE=TRUE -DBoost_NO_SYSTEM_PATHS=TRUE -DBOOST_ROOT=/home/USER1/boost_1_67_0/ -DBoost_LIBRARY_DIRS=/home/USER1/boost_1_67_0/stage/lib/ -DOPENSSL_INCLUDE_DIR=/home/USER1/openssl-1.0.2p/include -DOPENSSL_SSL_LIBRARY=/home/USER1/openssl-1.0.2p/libssl.so -DOPENSSL_CRYPTO_LIBRARY=/home/USER1/openssl-1.0.2p/libcrypto.so ..
    $ make -j4


## Boost updates (if it needs)

    $ wget https://dl.bintray.com/boostorg/release/1.67.0/source/boost_1_67_0.tar.gz
    $ tar -xzf boost_1_*
    $ cd boost_1_*
    $ ./bootstrap.sh --prefix=/usr
    $ sudo ./b2 install --prefix=/usr --with=all -j4
    

## Tested
1. Ubuntu 18.04
2. OSX 10.13

## How to use standalone

### Run RethinkDB in docker ###
    $ docker pull rethinkdb
    $ mkdir rethinkdb; cd rethinkdb
    $ docker run --name mile-rethink -v "$PWD:/data" -d -p 8080:8080 -p 28015:28015 -p 29015:29015 rethinkdb
    
### Launch index service ###
    $ # cd mile-block-indexer     
	$ ./build/mile-block-indexer --config=config.yaml

Change ```rethink_db_ip: "172.17.0.2"``` to real rethinkdb address. For example, if you build this instance of mile-block-indexer in docker you can connect to 172.17.0.1 as local service IP.

## How to build docker container

### Build base image ###
	$ docker build -f Dockerfile_base -t mile:base .

### Build app image ###
	$ docker build -f Dockerfile_app -t indexer:$(git log -1 --format=%h) .
