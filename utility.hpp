#pragma once

#include <unordered_set>

#include <json.hpp>

auto to_base58(std::string key_hex) -> std::string;

auto to_json(const std::string&block ) -> nlohmann::json;

bool in(const std::string &element, std::unordered_set<std::string> &container);